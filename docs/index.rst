.. ChatOps documentation master file, created by
   sphinx-quickstart on Sun Dec 23 14:08:33 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ChatOps's documentation!
===================================

.. note:: The project is moved to https://itpp.dev/chat/ and will be shutdowned here soon

.. toctree::
   :maxdepth: 2

   todo-bot/index
   resend-bot/index
   ifttt-to-telegram/index
